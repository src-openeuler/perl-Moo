Name:           perl-Moo
Version:        2.005005
Release:        3
Summary:        Minimalist Object Orientation (with Moose compatibility)
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Moo

Source0:        https://cpan.metacpan.org/authors/id/H/HA/HAARG/Moo-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  make perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.76 perl(strict)
BuildRequires:  perl(warnings) perl(B) perl(base) perl(Carp) perl(Class::Method::Modifiers) >= 1.10
BuildRequires:  perl(Config) perl(constant) perl(Devel::GlobalDestruction) >= 0.11 perl(Exporter) >= 5.57
BuildRequires:  perl(Import::Into) >= 1.002 perl(Module::Runtime) >= 0.014 perl(mro)
BuildRequires:  perl(overload) perl(Role::Tiny) >= 2.000004 perl(Scalar::Util) perl(strictures) >= 1.004003
BuildRequires:  perl(Sub::Defer) >= 2.003001 perl(Sub::Quote) >= 2.003001 perl(Class::XSAccessor) >= 1.18
BuildRequires:  perl(Sub::Name) perl(B::Deparse) perl(Class::XSAccessor::Array) perl(Data::Dumper)
BuildRequires:  perl(FindBin) perl(lib) perl(Test::Fatal) >= 0.003 perl(Test::More) >= 0.96 perl(threads)
BuildRequires:  perl(CPAN::Meta::Requirements)
Requires:       perl(Carp) perl(Class::Method::Modifiers) >= 1.10 perl(Devel::GlobalDestruction) >= 0.11
Requires:       perl(Import::Into) >= 1.002 perl(Module::Runtime) >= 0.012 perl(mro) perl(Role::Tiny) >= 1.003003

%description
Moo is an extremely light-weight Object Orientation system. It allows one to concisely define 
objects and roles with a convenient syntax that avoids the details of Perl's object system.  
Moo contains a subset of Moose and is optimised for rapid startup.

Moo avoids depending on any XS modules to allow for simple deployments. The name Moo is based 
on the idea that it provides almost -- but not quite -- two thirds of Moose. 

%package_help

%prep
%autosetup -n Moo-%{version} -p1

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc README
%{perl_vendorlib}/*

%files help
%doc Changes
%{_mandir}/man3/*

%changelog
* Tue Jan 21 2025 Funda Wang <fundawang@yeah.net> - 2.005005-3
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Jan 20 2025 sqfu <dev01203@linx-info.com> - 2.005005-2
- allow perl module dependencies

* Mon May 15 2023 Ge Wang <wang__ge@126.com> - 2.005005-1
- Upgrade to version 2.005005

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 2.005004-1
- Upgrade to version 2.005004

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.003004-8
- Package init
